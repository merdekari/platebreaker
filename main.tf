data "digitalocean_project" "merdekari" {
  name = "merdekari"
}

data "digitalocean_vpc" "sgp" {
  name = "default-sgp1"
}

resource "digitalocean_droplet" "platebreaker-01" {
  image    = "ubuntu-22-04-x64"
  name     = "platebreaker-01"
  region   = "sgp1"
  size     = "s-1vcpu-512mb-10gb"
  vpc_uuid = data.digitalocean_vpc.sgp.id
  ssh_keys = [
    # akhy's machines
    "0d:e5:ce:af:af:24:d1:13:84:5b:8f:81:1d:b5:f7:76", # akhyrul@gmail.com
    "1c:89:59:17:37:b0:77:71:8c:0c:8c:7e:c7:a8:b6:a8", # mihimaru
    "d9:e2:27:49:c9:8a:74:ea:3b:59:1d:68:d3:03:de:b6", # novelo
  ]

  lifecycle {
    ignore_changes = [
      ssh_keys
    ]
  }
}
