terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.21.0"
    }
  }

  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/38237030/terraform/state/platebreaker"
    lock_address   = "https://gitlab.com/api/v4/projects/38237030/terraform/state/platebreaker/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/38237030/terraform/state/platebreaker/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}
